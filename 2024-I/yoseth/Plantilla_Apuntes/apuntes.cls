\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{apuntesud}[2023/05/26 Clase personalizada]
\LoadClass[twoside,titlepage]{article}


% Paquetes necesarios.

\RequirePackage{fancyhdr}
\RequirePackage{amsmath,amsthm,amsfonts,amssymb,geometry} 
\RequirePackage{caption,graphicx,subfig}
\RequirePackage{float}
\RequirePackage{tcolorbox}
\RequirePackage[spanish]{babel}
\RequirePackage[colorlinks=true]{hyperref}
\RequirePackage[title]{appendix}
\RequirePackage{enumitem}

% Paquetes para las fuentes
\RequirePackage{mlmodern}
\RequirePackage[T1]{fontenc}

% Nuevos estilos para los teoremas y las definiciones

\newtheoremstyle{thmstyle}
{\topsep}
{\topsep}
{\slshape}
{}
{\bfseries}
{.}
{0.5em}
{}
\newtheoremstyle{definitionstyle}
{\topsep}
{\topsep}
{}
{}
{\bfseries}
{.}{0.5em}
{}

% Redefinimos el estilo para las teoremas importantes de los apuntes.

\theoremstyle{thmstyle}
\newtheorem{theorem}{Teorema}[section]
\newtheorem{lemma}[theorem]{Lema}
\newtheorem{proposition}[theorem]{Proposición}
\newtheorem{corollary}[theorem]{Corolario}

% Redefinimos el estilo para las definiciones, problemas y ejemplos

\theoremstyle{definitionstyle}
\newtheorem{definition}[theorem]{Definición}
\newtheorem*{ejercicio}{Ejercicio}
\newtheorem*{ejemplo}{Ejemplo}
\theoremstyle{remark}
\newtheorem*{remark}{Nota}


% Margen y encabezados
\linespread{1.1} 

\setlength{\parskip}{10pt}

\setlength{\parindent}{0pt}

\hypersetup{linkcolor=blue}

\geometry{a4paper,left=3.14cm,right=3.14cm,top=2.54cm,bottom=2.54cm}

% Numero de subsecciones que aparecen en la tabla de contenidos.
\setcounter{tocdepth}{2}

% Configuración de página
\pagestyle{fancy}
\fancyhf{} 
\fancyhead[C]{\leftmark}
\fancyfoot[C]{\thepage}
\renewcommand{\headrulewidth}{0.3mm} 
\renewcommand{\footrulewidth}{0mm}

