\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{talllerplantilla}[2024/05/28]
\LoadClass[twoside]{article}

\RequirePackage{fancyhdr}
\RequirePackage{amsmath,amsthm,amsfonts,amssymb,geometry} 
\RequirePackage{caption,graphicx,subfig}
\RequirePackage{float}
\RequirePackage{tcolorbox}
\RequirePackage[spanish]{babel}
\RequirePackage[colorlinks=true]{hyperref}
\RequirePackage[title]{appendix}
\RequirePackage{hyperref}
\RequirePackage{enumitem}

\RequirePackage{lmodern} 

\RequirePackage[T1]{fontenc}

%--ESTILO DE LOS TEOREMAS Y TEOREMAS
\newtheoremstyle{thmstyle}
{\topsep}%   espacio abajo
{\topsep}%   espacio arriba
{\slshape}%  fuente del cuerpo
{}%          %sangria si= \parindent no=empty
{\bfseries}% fuente de la cabeza del thm
{.}%         Punctuation after thm head
{0.5em}%     Space after thm head: " " = normal interword space;
%         \newline = linebreak
{}%          Thm head spec (can be left empty, meaning `normal')
\newtheoremstyle{defstyle}{\topsep}{\topsep}{}{}{\bfseries}{.}{0.5em}{}
\newtheoremstyle{ejerstyle}{\topsep}{\topsep}{}{}{\bfseries}{.}{\newline}{}



\theoremstyle{thmstyle}
\newtheorem{theorem}{Teorema}[section]
\newtheorem{lemma}[theorem]{Lema} %Ambiente lema
\newtheorem{proposition}[theorem]{Proposición} %Ambiente proposicion
\newtheorem{corollary}[theorem]{Corolario} %Ambiente corolario

\theoremstyle{defstyle} %Estilo para definiciones
\newtheorem{definition}[theorem]{Definición} %Ambiente definición
\newtheorem*{ejemplo}{Ejemplo}


\theoremstyle{ejerstyle}
\newtheorem{ejercicio}{\large Ejercicio}
\theoremstyle{remark} %Estilo para aclaraciones o notas
\newtheorem{remark}{Nota}

%---------------------------------------------------------------------------
% MARGEN Y ENCABEZADOS 
\linespread{1.1} 
\setlength{\parskip}{10pt}
\setlength{\parindent}{0pt}% Palladio needs more leading (space between lines)
\hypersetup{linkcolor=blue}
\geometry{a4paper,left=3.14cm,right=3.14cm,top=2.54cm,bottom=2.54cm}
%设置页眉
\pagestyle{fancy}
\fancyhf{} 
\fancyhead[C]{\leftmark}
\fancyfoot[C]{\thepage}
\renewcommand{\headrulewidth}{0.3mm} 
\renewcommand{\footrulewidth}{0mm}
%----------------------------------------------------------------------------------


