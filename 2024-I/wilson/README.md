# Análisis de Series de Tiempo de Precios del Café
Este proyecto utiliza el software libre R para analizar la serie de tiempo de precios del café. 
Se realizan diversas operaciones, como la creación de series de tiempo, la partición de datos por año, el cálculo de variaciones, 
la descomposición de las series de tiempo y la evaluación de la estacionariedad.
