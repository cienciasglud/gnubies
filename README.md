# GLUD Ciencias GNUBIES

Bienvenidos al Grupo GNU/Linux Universidad Distrital Ciencias


## Proyectos:

- [2024-I](https://gitlab.com/cienciasglud/gnubies/-/tree/main/2024-I?ref_type=heads)

## Misión

Nuestra misión en el Grupo GNU/Linux de la Universidad Distrital (G.L.U.D.) extensión Ciencias es fomentar el uso de tecnologías libres en el entorno académico y promover su enseñanza entre los estudiantes para que desarrollen proyectos que reflejen sus intereses individuales y además que se alineen con los intereses de la Facultad de Ciencias. Brindamos un entorno de aprendizaje colaborativo donde se fomenta la innovación y se desarrollan habilidades en tecnologías libres, con el fin de impulsar el crecimiento académico de nuestros miembros.

## Visión  

Nos visualizamos como un espacio de integración, basado en el respeto y la cooperación, donde estudiantes y profesores de distintas carreras dentro de la Facultad de Ciencias, así como de otras áreas de la Universidad, trabajen juntos en el desarrollo de proyectos innovadores. Buscamos ser un referente de colaboración mutua y de apoyo a la comunidad universitaria, contribuyendo al avance de proyectos y la creación de soluciones que impacten positivamente.

